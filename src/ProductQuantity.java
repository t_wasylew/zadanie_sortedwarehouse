import java.time.LocalDateTime;

public class ProductQuantity implements Comparable<ProductQuantity> {
    private Product product;
    private int quantity;

    public ProductQuantity(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void update(int addedQuantity, Product updatedProduct) {
        System.out.println("Aktualizuję produktu przed: " + this);
        quantity += addedQuantity;
        product.setPrice(updatedProduct.getPrice()); // aktualizacja ceny
        product.setExpirationDate(updatedProduct.getExpirationDate()); // aktualizacja daty waznosci
        System.out.println("Aktualizuję produktu po: " + this);
    }

    public void update(int quantityToSubtract) {
        if (quantity < quantityToSubtract) {
            // niewystarczająca ilość
            int difference = quantityToSubtract - quantity;
            System.out.println("Brakuje " + difference + " produktów tego typu.");
            quantity = 0;
        } else {
            quantity -= quantityToSubtract;
        }
        System.out.println("Zmniejszyłem ilość produktu: " + this);
    }

    @Override
    public String toString() {
        return "ProductQuantity{" +
                "product=" + product +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public int compareTo(ProductQuantity p2) {
        LocalDateTime expirationDateP1 = getProduct().getExpirationDate();
        LocalDateTime expirationDateP2 = p2.getProduct().getExpirationDate();
        boolean equal = expirationDateP1.equals(expirationDateP2);
        if (equal) {
            return 0;
        } else {
            if (expirationDateP1.isAfter(expirationDateP2)) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
